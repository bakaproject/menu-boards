var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Slide = new keystone.List('Slide', {
    autokey: { path: 'slug', from: 'title', unique: true },
    map: { name: 'title' },
    defaultSort: '-createdAt'
});

Slide.add({
    title:                  { type: String, required: true },
    state:                 { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
    author:                { type: Types.Relationship, ref: 'User' },
    createdAt:             { type: Date, default: Date.now },
    publishedAt:           { type: Date, default: Date.now },
    slideType:             { type: Types.Select, options: 'dish', default: 'dish'},
    dishName:              { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    nutritional:           { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    dishCategory:          { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    dishDescription:       { type: Types.Textarea, dependsOn: { slideType: ['dish'], type: String } },
    mainImage:             { type: Types.CloudinaryImages, dependsOn: { slideType: ['dish'], type: String } },
    ingredient1:           { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    ingredient1Icon:       { type: Types.CloudinaryImages, dependsOn: { slideType: ['dish'], type: String } },
    ingredient1TextColor:  { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    ingredient2:           { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    ingredient2Icon:       { type: Types.CloudinaryImages, dependsOn: { slideType: ['dish'], type: String } },
    ingredient2TextColor:  { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    ingredient3:           { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    ingredient3Icon:       { type: Types.CloudinaryImages, dependsOn: { slideType: ['dish'], type: String } },
    ingredient3TextColor:  { type: Types.Text, dependsOn: { slideType: ['dish'], type: String } },
    featuredDish1Image:    { type: Types.CloudinaryImages, dependsOn: { slideType: ['title'], type: String } },
    featuredDish1Title:    { type: Types.Text, dependsOn: { slideType: ['title'], type: String } },
    featuredDish2Image:    { type: Types.CloudinaryImages, dependsOn: { slideType: ['title'], type: String } },
    featuredDish2Title:    { type: Types.Text, dependsOn: { slideType: ['title'], type: String } },
    featuredDish3Image:    { type: Types.CloudinaryImages, dependsOn: { slideType: ['title'], type: String } },
    featuredDish3Title:    { type: Types.Text, dependsOn: { slideType: ['title'], type: String } }

});

Slide.defaultColumns = 'title, state|20%, author, publishedAt|15%';
Slide.register();