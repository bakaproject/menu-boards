var keystone = require('keystone'),
    Types = keystone.Field.Types;

var Board = new keystone.List('Board', {
    autokey: { path: 'slug', from: 'title', unique: true },
    map: { name: 'title' },
    defaultSort: '-createdAt'
});

Board.add({
    boardNumber:     {type: Number},
    title:           { type: String, required: true },
    state:           { type: Types.Select, options: 'draft, published, archived', default: 'draft' },
    author:          { type: Types.Relationship, ref: 'User' },
    createdAt:       { type: Date, default: Date.now },
    publishedAt:     { type: Date, default: Date.now },
    slide1:          { type: Types.Relationship, ref: 'Slide'},
    slide2:          { type: Types.Relationship, ref: 'Slide'},
    slide3:          { type: Types.Relationship, ref: 'Slide'},
    slide4:          { type: Types.Relationship, ref: 'Slide'},
    slide5:          { type: Types.Relationship, ref: 'Slide'},
    slide6:          { type: Types.Relationship, ref: 'Slide'}
    
});

Board.defaultColumns = 'title, state|20%, author, publishedAt|15%';
Board.register();