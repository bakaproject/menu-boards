// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').load();

// Require keystone
var keystone = require('keystone');

// Initialise Keystone with your project's configuration.
// See http://keystonejs.com/guide/config for available options
// and documentation.

keystone.init({

	'name': 'Menu Boards',
	'brand': 'Menu Boards',

	'sass': 'public',
	'static': 'public',
	'favicon': 'public/favicon.ico',
	'views': 'templates/views',
	'view engine': 'jade',

	'auto update': true,
	'session': true,
	'auth': true,
	'user model': 'User',

	'cloudinary config': 'cloudinary://625398563727215:HBUbAm93d9imjbeX-umcVmtQQHA@mandoboards',
	'mongo': 'mongodb://heroku_ldzl1hmr:k0cdcjmbe3gir883kat0er28mg@ds019063.mlab.com:19063/heroku_ldzl1hmr'

});


// Load your project's Models

keystone.import('models');

// Setup common locals for your templates. The following are required for the
// bundled templates and layouts. Any runtime locals (that should be set uniquely
// for each request) should be added to ./routes/middleware.js

keystone.set('locals', {
	_: require('underscore'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable
});

// Load your project's Routes

keystone.set('routes', require('./routes'));

// Configure the navigation bar in Keystone's Admin UI

keystone.set('nav', {
	'users': 'users'
});

// if(env.MONGODB_URI) {
// 	console.log('mongo!!!!');
// 	keystone.set({'mongoose': env.MONGODB_URI});
// } else {
// 	console.log('no mongo');
// }

// Cloudinary creds
// keystone.set('cloudinary config': 'cloudinary://625398563727215:HBUbAm93d9imjbeX-umcVmtQQHA@mandoboards');


// Start Keystone to connect to your database and initialise the web server

keystone.start();
