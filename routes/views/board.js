var keystone = require('keystone');
var Board = keystone.list('Board');

exports = module.exports = function(req, res) {

	var view = new keystone.View(req, res);
  var locals = res.locals;

  // Init locals
  locals.filters = {
    boardID: req.params.id
  };

  locals.params = {
    scaled: (req.params.scaled) ? true : false
  };


  // Load the current board
  view.on('init', function (next) {

    var board = Board.model.find().where('slug', locals.filters.boardID).populate('slide1 slide2 slide3 slide4 slide5 slide6');

    board.exec(function (err, result) {
      locals.boardData = result[0];
      if(err) {
        console.log(err);
      } else {
        console.log('Board ' + locals.filters.boardID + ' Loaded');
      }
      next(err);
    });
  });

	view.render('board');

};
