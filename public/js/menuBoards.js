var menuBoards = menuBoards || {};

menuBoards.interval = 7;
menuBoards.slideTime = 1000 * menuBoards.interval;
menuBoards.slideNumber = 1;
menuBoards.slideContainer = $('.slideContainer');
menuBoards.slides = $('.slideContainer > li');
menuBoards.containerHeight = menuBoards.slides.length * $('.boardContainer')[0].getBoundingClientRect().height;
menuBoards.multiplier = ($('body').hasClass('scaled')) ? 3.125 : 1;


menuBoards.animateBoard = function() {
  menuBoards.slideContainer.css({'height' : menuBoards.containerHeight * menuBoards.multiplier + 1});
  menuBoards.slideTransition();
};


menuBoards.slideTransition = function() {
  var slideTime = 1000 * menuBoards.interval;
  var position = (-(menuBoards.slideNumber) * (menuBoards.containerHeight / menuBoards.slides.length)) * menuBoards.multiplier;
  var slideCheck = (menuBoards.slideNumber - 1 == 0) ? 1 : menuBoards.slideNumber - 1;
  var previousSlide = $(menuBoards.slideContainer).find('[data-slideId="' + slideCheck + '"]');
  var currentSlide = $(menuBoards.slideContainer).find('[data-slideId="' + menuBoards.slideNumber + '"]');

  // console.log(slideCheck, previousSlide, currentSlide);

  if (menuBoards.slideNumber == (menuBoards.slides.length - 1)) {
    var anim2 = new TimelineMax();
    anim2.to(menuBoards.slideContainer.find('.detail'), 0.5, {autoAlpha: 0, ease:Circ})
      // .set(menuBoards.slideContainer.find('.detail'), {right: '-51%', ease:Circ})
      .to(menuBoards.slideContainer, 0.75, {top: position, ease:Circ})
      .to(menuBoards.slideContainer.find('.detail'), 0.7, {autoAlpha: 1, ease:Circ})
      // .to(menuBoards.slideContainer.find('.detail'), 0.7, {right: '0', ease:Circ});
      .set(menuBoards.slideContainer, {top: 0});
    menuBoards.slideNumber = 1;
  } else {
    console.log(previousSlide.find('.detail'), currentSlide.find('.detail'));
    var anim1 = new TimelineMax();
    anim1.to(menuBoards.slideContainer.find('.detail'), 0.5, {autoAlpha: 0, ease:Circ})
      // .set(menuBoards.slideContainer.find('.detail'), {right: '-51%', ease:Circ})
      .to(menuBoards.slideContainer, 0.75, {top: position, ease:Circ})
      .to(menuBoards.slideContainer.find('.detail'), 0.7, {autoAlpha: 1, ease:Circ})
      // .to(menuBoards.slideContainer.find('.detail'), 0.7, {right: '0', ease:Circ});
    menuBoards.slideNumber++;
  }

  setTimeout(function(){
    menuBoards.slideTransition();
  }, menuBoards.slideTime);
};